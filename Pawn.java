package chess;

import chess.ChessPiece;
import chess.IChessPiece;
import chess.Move;
import chess.Player;

/*****************************************************************
 Sets the parameters for the Pawn piece for the game of chess
 @author 	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class Pawn extends ChessPiece {

	/*****************************************************************
	 Constructor that inherits the player passed in by the super class ChessPiece
	 @param	player	Enum player that represents a Players color
	 *****************************************************************/
	public Pawn(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the Pawn ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "Pawn";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the Pawn chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = false;

		if(super.isValidMove(move, board) == false){
			return false;
		}

		// Checks if the destination tile is empty or is of the same 'color' , needs to change value to 2 spaces if it's the first turn and has a valid attack that can be made in one of the two forward diagonal tiles.
		switch (board[move.fromRow][move.fromColumn].player()) {
			case WHITE:
				// If a white pawn is in the starting row gives the option for one or two moves, otherwise move one space forward if the path is not blocked
				if(board[move.toRow][move.toColumn] == null) {
					if (move.fromRow == 6 && move.toRow == move.fromRow - 1 && move.toColumn == move.fromColumn) {
						valid = true;
					} else if(move.fromRow == 6 && move.toRow == move.fromRow - 2 && move.toColumn == move.fromColumn && board[move.fromRow - 1][move.fromColumn] == null){
						valid = true;
					} else if (move.toRow == move.fromRow - 1 && move.fromColumn == move.toColumn) {
						valid = true;
					}
				}
				// If a black piece is on either of the front two corners of the white pawn allow the piece to make a valid attack
				else if(move.toRow == move.fromRow - 1 && Math.abs(move.toColumn - move.fromColumn) == 1 && board[move.toRow][move.toColumn].player() == Player.BLACK)
				{
					valid = true;
				}
				break;
			case BLACK:
				// If a white pawn is in the starting row gives the option for one or two moves, otherwise move one space forward if the path is not blocked
				if(board[move.toRow][move.toColumn] == null) {
					if (move.fromRow == 1 && move.toRow == move.fromRow + 1 && move.toColumn == move.fromColumn) {
						valid = true;
					} else if(move.fromRow == 1 && move.toRow == move.fromRow + 2 && move.toColumn == move.fromColumn && board[move.fromRow + 1][move.fromColumn] == null){
						valid = true;
					} else if (move.toRow == move.fromRow + 1 && move.fromColumn == move.toColumn) {
						valid = true;
					}
				}
				// If a white piece is on either of the front two corners of the black pawn allow the piece to make a valid attack
				else if(move.toRow == move.fromRow + 1 && Math.abs(move.toColumn - move.fromColumn) == 1 && board[move.toRow][move.toColumn].player() == Player.WHITE){
					valid = true;
				}
		}
		return valid;
	}

}
