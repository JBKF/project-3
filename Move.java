package chess;

/*****************
 * DO NOT CHANGE
 *****************/
public class Move {
	/** Instance Variables */
	public int fromRow, fromColumn, toRow, toColumn;

	/*****************************************************************
	 Default Constructor
	 *****************************************************************/
	public Move() {
	}

	/*****************************************************************
	 Constructor with four parameters that represent piece coordinates on the chess board
	 @param	fromRow	integer that represents the row of the selected ChessPiece
	 @param fromColumn integer that represents the column of the selected ChessPiece
	 @param toRow integer that represents the desired row a ChessPiece wants to move to
	 @param toColumn integer that represents the desired column a ChessPiece wants to move to
	 *****************************************************************/
	public Move(int fromRow, int fromColumn, int toRow, int toColumn) {
		this.fromRow = fromRow;
		this.fromColumn = fromColumn;
		this.toRow = toRow;
		this.toColumn = toColumn;
	}

	/*****************************************************************
	 Utilizes the parameters and formats those parameters into a String
	 @return	String that represents the move of the chess board (starting and ending coordinates)
	 *****************************************************************/
	@Override
	public String toString() {
		return "Move [fromRow=" + fromRow + ", fromColumn=" + fromColumn + ", toRow=" + toRow + ", toColumn=" + toColumn
				+ "]";
	}
	
	
}
