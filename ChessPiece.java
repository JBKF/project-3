package chess;

/*****************************************************************
 ChessPiece that represents a generic piece on the chess board
 @author	Jordan Brown, Kendra Francis
 @version
 *****************************************************************/
public abstract class ChessPiece implements IChessPiece {
	/** Instance Variables */

	/** Player Object in which owns the chess piece */
	private Player owner;

	/*****************************************************************
	 Constructor that sets the owner of the current piece
	 @param	player	The owner of the ChessPiece
	 *****************************************************************/
	protected ChessPiece(Player player) {
		this.owner = player;
	}

	/*****************************************************************
	 Returns the type of piece that is being used in a String
	 @return
	 *****************************************************************/
	public abstract String type();

	/*****************************************************************
	 Gets the player color of the current ChessPiece
	 @return	The player color of the owner of the ChessPiece
	 *****************************************************************/
	public Player player() {
		return owner;
	}

	/*****************************************************************
	 Checks if the move performed meets the specified parameters
	 @param	move	Object used to pass in coordinates for the to and from destinations of a ChessPiece
	 @param board	IChessPiece used to verify ChessPiece or spacial coordinates
	 @return	boolean value that represents the validity of the passed in Move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = false;

		// Verifies that the starting and ending locations are different
		if (((move.fromRow == move.toRow) && (move.fromColumn == move.toColumn)) == false) {
			// Verifies that the destination space does not contain a piece
			if(board[move.toRow][move.toColumn] == null) {
				valid = true;
			}
			// Verifies that if a piece is located in the destination space, then that piece is not the same color as the piece being moved
			else if(board[move.fromRow][move.fromColumn].player() != board[move.toRow][move.toColumn].player()) {
				valid = true;
			}
		}

		// Return Statement
		return valid;
	}
}
