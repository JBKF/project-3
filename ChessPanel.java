package chess;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*****************************************************************
 ChessPanel
 @author    Jordan Brown, Kendra Francis
 @version   1.0
 *****************************************************************/
public class ChessPanel extends JPanel {
    /** Instance Variables */

    /** 2D JButton array that represents the chess board */
    private JButton[][] board;
    /** Represents the game of chess being played and used to access game logic */
    private ChessModel model;

    /** JButton that represents the undo button used to undo the previously made move */
    private JButton undo;

    /** ImageIcons that represent player white pieces */
    private ImageIcon wRook;
    private ImageIcon wBishop;
    private ImageIcon wQueen;
    private ImageIcon wKing;
    private ImageIcon wPawn;
    private ImageIcon wKnight;

    /** ImageIcons that represent player black pieces */
    private ImageIcon bRook;
    private ImageIcon bBishop;
    private ImageIcon bQueen;
    private ImageIcon bKing;
    private ImageIcon bPawn;
    private ImageIcon bKnight;

    /** Verifies if the first move of the game has been made */
    private boolean firstTurnFlag;

    /** Variables used to check the coordinates specified in a move */
    private int fromRow;
    private int toRow;
    private int fromCol;
    private int toCol;

    /** listener for the JButtons on the chess board */
    private listener listener;

    /*****************************************************************
    Default constructor which sets the initial state of the displayed board
    *****************************************************************/
    public ChessPanel() {
        // Variables used to add components to the ChessPanel
        model = new ChessModel();
        board = new JButton[model.numRows()][model.numColumns()];
        listener = new listener();
        undo = new JButton("Undo");

        // Set the ImageIcon values for the different ChessPieces
        createIcons();

        // Two panels that represents the location of the board and buttons
        JPanel boardPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        //Add an ActionListener to the undo button and add the undo button to the buttonPanel
        undo.addActionListener(listener);
        buttonPanel.add(undo);

        // Set the GridLayout which represents the chess board and its specified dimensions
        boardPanel.setLayout(new GridLayout(model.numRows(), model.numColumns(), 1, 1));
        boardPanel.setPreferredSize(new Dimension(600, 600));

        // Iterate through the 2D array that represents the chess board and create JButtons for each empty space and add ActionListeners to each of those
        for (int r = 0; r < model.numRows(); r++) {
            for (int c = 0; c < model.numColumns(); c++) {
                if (model.pieceAt(r, c) == null) {
                    board[r][c] = new JButton("", null);
                    board[r][c].addActionListener(listener);
                }
                else{
                    // If the space is not empty, place the proper piece in its starting space
                    placePieces(r, c);
                }
                // Adds the piece to the ChessPanel and sets its background color
                setBackGroundColor(r, c);
                boardPanel.add(board[r][c]);
            }
        }
        // Add the chess board to the ChessPanel and specify its location
        add(boardPanel, BorderLayout.WEST);
        // Add the button panel to the ChessPanel
        add(buttonPanel);
        // Specify that it's the first turn of the chess game
        firstTurnFlag = true;
        // Tell the player(s) whose turn it is
        System.out.println("Player " +model.currentPlayer()+ " is up.");
    }

    /*****************************************************************
     Set the background color of the board
     @param r - rows of the board
     @param c - cols of the board
     *****************************************************************/
    private void setBackGroundColor(int r, int c) {
        if ((c % 2 == 1 && r % 2 == 0) || (c % 2 == 0 && r % 2 == 1)) {
            board[r][c].setBackground(Color.LIGHT_GRAY);
        } else if ((c % 2 == 0 && r % 2 == 0) || (c % 2 == 1 && r % 2 == 1)) {
            board[r][c].setBackground(Color.WHITE);
        }
    }

    /*****************************************************************
     Place the pieces on the board after checking for the
     corresponding player
     @param r - rows of the board
     @param c - cols of the board
     *****************************************************************/
    private void placePieces(int r, int c) {
        if(model.pieceAt(r, c).player() == null){
            System.out.println("You have entered an invalid player");
        }
        else{
            switch(model.pieceAt(r, c).type())
            {
            case "Pawn":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wPawn);
                }
                else
                {
                    board[r][c] = new JButton(null, bPawn);
                }
                board[r][c].addActionListener(listener);
                break;
            case "Rook":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wRook);
                }
                else
                {
                    board[r][c] = new JButton(null, bRook);
                }
                board[r][c].addActionListener(listener);
                break;
            case "Knight":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wKnight);
                }
                else {
                    board[r][c] = new JButton(null, bKnight);
                }
                board[r][c].addActionListener(listener);
                break;
            case "Bishop":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wBishop);
                }
                else {
                    board[r][c] = new JButton(null, bBishop);
                }
                board[r][c].addActionListener(listener);
                break;
            case "Queen":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wQueen);
                }
                else {
                    board[r][c] = new JButton(null, bQueen);
                }
                board[r][c].addActionListener(listener);
                break;
            case "King":
                if(model.pieceAt(r, c).player() == Player.WHITE) {
                    board[r][c] = new JButton(null, wKing);
                }
                else {
                    board[r][c] = new JButton(null, bKing);
                }
                board[r][c].addActionListener(listener);
                break;
            }
        }
    }

    /*****************************************************************
     Creates the ImageIcon Instance Variables
     *****************************************************************/
    private void createIcons() {
        // Sets the ImageIcon filepath for all white ChessPieces
        wRook = new ImageIcon("./images/wRook.png");
        wBishop = new ImageIcon("./images/wBishop.png");
        wQueen = new ImageIcon("./images/wQueen.png");
        wKing = new ImageIcon("./images/wKing.png");
        wPawn = new ImageIcon("./images/wPawn.png");
        wKnight = new ImageIcon("./images/wKnight.png");

        // Sets the ImageIcon filepath for black ChessPieces
        bRook = new ImageIcon("./images/bRook.png");
        bBishop = new ImageIcon("./images/bBishop.png");
        bQueen = new ImageIcon("./images/bQueen.png");
        bKing = new ImageIcon("./images/bKing.png");
        bPawn = new ImageIcon("./images/bPawn.png");
        bKnight = new ImageIcon("./images/bKnight.png");
    }

    /*****************************************************************
     Displays the current state of the board by setting the
     corresponding ImageIcons
     *****************************************************************/
    private void displayBoard()
    {
        // Iterate through the 2D array that represents the chess board
        for (int r = 0; r < 8; r++)
        {
            for (int c = 0; c < 8; c++)
            {
                // Set the corresponding ImageIcons for spaces that hold a null value
                if (model.pieceAt(r, c) == null) {
                    board[r][c].setIcon(null);
                }
                // Set the corresponding ImageIcons for each player based on the ChessPiece type()
                else {
                    switch(model.pieceAt(r, c).type())
                    {
                        case "Pawn":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wPawn);
                            }
                            else
                            {
                                board[r][c].setIcon(bPawn);
                            }
                            break;
                        case "Rook":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wRook);
                            }
                            else
                            {
                                board[r][c].setIcon(bRook);
                            }
                            break;
                        case "Knight":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wKnight);
                            }
                            else {
                                board[r][c].setIcon(bKnight);
                            }
                            break;
                        case "Bishop":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wBishop);
                            }
                            else {
                                board[r][c].setIcon(bBishop);
                            }
                            break;
                        case "Queen":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wQueen);
                            }
                            else {
                                board[r][c].setIcon(bQueen);
                            }
                            break;
                        case "King":
                            if(model.pieceAt(r, c).player() == Player.WHITE) {
                                board[r][c].setIcon(wKing);
                            }
                            else {
                                board[r][c].setIcon(bKing);
                            }
                            break;
                    }
                }
            }
        }
        repaint();
    }

    // inner class that represents action listener for buttons
    private class listener implements ActionListener {
        /*****************************************************************
         Performs an action based on the event passed in as the parameter.
         @param event - Event that is listening on each JButton variable
         *****************************************************************/
        public void actionPerformed(ActionEvent event) {
            // Check if the event was triggered by the undo button
            if(undo == event.getSource()) {
                // Perform the undo action for the current ChessModel
                model.undo();

                for (int r = 0; r < model.numRows(); r++) {
                    // Iterates through the cols of the chess board
                    for (int c = 0; c < model.numColumns(); c++) {
                        if(model.pieceAt(r, c) != null){
                            System.out.println("Player: " + model.pieceAt(r, c).player() + " Piece: " + model.pieceAt(r, c).type() + " Row: " + r + " Col: " + c);
                        }
                    }
                }

                // Display the updated board
                displayBoard();
            }

            // Iterates through the rows of the chess board
            for (int r = 0; r < model.numRows(); r++) {
                // Iterates through the cols of the chess board
                for (int c = 0; c < model.numColumns(); c++) {



                    // Check to see if clicked location is in the same location as the event
                    if (board[r][c] == event.getSource()) {
                        // After the first click it is no longer the first turn of the iteration 'firstTurnFlag'
                        if (firstTurnFlag == true  ) { //|| model.pieceAt(r, c).player() == player
                            // Sets the starting coordinates of a ChessPiece
                            fromRow = r;
                            fromCol = c;

                            //System.out.println("");

                            // Checks if the piece is valid (move elsewhere)
                            if(model.pieceAt(fromRow, fromCol) != null && model.pieceAt(fromRow, fromCol).player() == model.currentPlayer()) {
                                firstTurnFlag = false;
                                System.out.println("Piece Selected: " + model.pieceAt(r, c).player() + " " + model.pieceAt(r, c).type() + " Row: " + fromRow + " Col: " + fromCol);
                            }
                            else{
                                System.out.println("Invalid Piece.");
                            }
                        }
                        else {
                            /*
                            if(model.pieceAt(fromRow, fromCol).player() == model.pieceAt(r, c).player())
                            {
                                fromRow = r;
                                fromCol = c;
                                System.out.println("Piece Selected: " + model.pieceAt(r, c).player() + " " + model.pieceAt(r, c).type() + " Row: " + fromRow + " Col: " + fromCol);
                                displayBoard();
                                return;
                                //board[r][c].setSelected(true);
                                //firstTurnFlag = false;
                                //return;
                            }
                            */

                            //Sets the destination of the piece movement
                            toRow = r;
                            toCol = c;

                            // Create a new move with the coordinates specified by the user input
                            Move m = new Move(fromRow, fromCol, toRow, toCol);

                            // Check the validity of the move that was just created
                            if (model.isValidMove(m) == true) {
                                // Perform the move
                                model.move(m);

                                if(model.inCheck(model.currentPlayer()) == true){
                                    JOptionPane.showMessageDialog(null, "Invalid Move. Move puts Player " + model.currentPlayer() + " in check.");
                                    firstTurnFlag = true;
                                    model.undo();
                                    return;
                                }
                                // Display the updated board
                                displayBoard();
                                // Change the player, as the move has been performed
                                model.setNextPlayer();
                                //player = model.currentPlayer();

                                // Checks if the player whose turn it now is, is in check
                                if(model.inCheck(model.currentPlayer()) == true){
                                    // Message to be outputted if the player is in check
                                    JOptionPane.showMessageDialog(null, "Player " + model.currentPlayer() + " is in check.");
                                }
                                // Resets the move back to the first turn
                                firstTurnFlag = true;

                                // Outputs the color of the player whose turn it has no become to the console
                                System.out.println(model.currentPlayer() + " is now up.");
                            }
                            else
                            {
                                // Output to the console in the instance of an invalid move
                                System.out.println("Invalid Move.");
                            }
                        }
                    }
                }
            }
        }
    }
}
