package chess;

/*****************************************************************
 Sets the parameters for the Queen piece for the game of chess
 @author	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class Queen extends ChessPiece {

	/*****************************************************************
	 Constructor that inherits the player passed in by the super class ChessPiece
	 @param	player	Enum player that represents a Players color
	 *****************************************************************/
	public Queen(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the Queen ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "Queen";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the Queen chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		Bishop move1 = new Bishop(board[move.fromRow][move.fromColumn].player());
		Rook move2 = new Rook(board[move.fromRow][move.fromColumn].player());
		if(super.isValidMove(move, board) == true){
			return (move1.isValidMove(move, board) || move2.isValidMove(move, board));
		}
		else{
			return false;
		}
	}
}
