package chess;

import chess.ChessPiece;
import chess.IChessPiece;
import chess.Move;
import chess.Player;

/*****************************************************************
 Sets the parameters for the King piece for the game of chess
 @author	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class King extends ChessPiece {

	/*****************************************************************
	 Constructor that inherits the player passed in by the super class ChessPiece
	 @param	player	Enum player that represents a Players color
	 *****************************************************************/
	public King(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the King ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "King";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the King chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = false;

		// Allows the king piece to move in any location 1 space away outside of the initial position in which is taken care of in the abstract ChessPiece class
		if (super.isValidMove(move, board) && Math.abs(move.toRow - move.fromRow) <= 1 && Math.abs(move.toColumn - move.fromColumn) <= 1){
			valid = true;
		}
		return valid;
	}
}
