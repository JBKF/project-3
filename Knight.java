package chess;

import chess.ChessPiece;
import chess.IChessPiece;
import chess.Move;
import chess.Player;

/*****************************************************************
 Sets the parameters for the Knight piece for the game of chess
 @author	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class Knight extends ChessPiece {

	/*****************************************************************
	Constructor that inherits the player passed in by the super class ChessPiece
	@param	player	Enum player that represents a Players color
	 *****************************************************************/
	public Knight(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the Knight ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "Knight";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the Knight chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board){
		boolean valid = false;

		// The destination space must be within 3 total spaces from the space the piece is currently occupying
		// The destination row must be 1 or 2 rows away, and the columns are 1 or 2 columns away
		if(super.isValidMove(move, board) && Math.abs(move.toRow - move.fromRow) == 2 && Math.abs(move.toColumn - move.fromColumn) == 1){
			valid = true;
		}
		else if(super.isValidMove(move, board) && Math.abs(move.toRow - move.fromRow) == 1 && Math.abs(move.toColumn - move.fromColumn) == 2){
			valid = true;
		}

		return valid;
		
	}

}
