package chess;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessTest {


    // Test for the default Constructor and the pieceAt method
    @Test
    void pieceAt(){
        ChessModel m1 = new ChessModel();
        for(int row = 0; row < m1.numRows(); row++) {
            for (int column = 0; column < m1.numColumns(); column++) {
                switch (row) {
                    case 0:
                        assertEquals(Player.BLACK, m1.pieceAt(row, column).player());
                        if (column == 0 || column == 7) {
                            assertEquals("Rook", m1.pieceAt(row, column).type());
                        } else if (column == 1 || column == 6) {
                            assertEquals("Knight", m1.pieceAt(row, column).type());
                        } else if (column == 2 || column == 5) {
                            assertEquals("Bishop", m1.pieceAt(row, column).type());
                        } else if (column == 3) {
                            assertEquals("Queen", m1.pieceAt(row, column).type());
                        } else {
                            assertEquals("King", m1.pieceAt(row, column).type());
                            assertEquals(4, column);
                        }
                        break;
                    case 1:
                        assertEquals(Player.BLACK, m1.pieceAt(row, column).player());
                        assertEquals("Pawn", m1.pieceAt(row, column).type());
                        break;
                    case 6:
                        assertEquals("Pawn", m1.pieceAt(row, column).type());
                        assertEquals(Player.WHITE, m1.pieceAt(row, column).player());
                        break;
                    case 7:
                        assertEquals(Player.WHITE, m1.pieceAt(row, column).player());
                        if (column == 0 || column == 7) {
                            assertEquals("Rook", m1.pieceAt(row, column).type());
                        } else if (column == 1 || column == 6) {
                            assertEquals("Knight", m1.pieceAt(row, column).type());
                        } else if (column == 2 || column == 5) {
                            assertEquals("Bishop", m1.pieceAt(row, column).type());
                        } else if (column == 3) {
                            assertEquals("Queen", m1.pieceAt(row, column).type());
                        } else {
                            assertEquals("King", m1.pieceAt(row, column).type());
                            assertEquals(4, column);
                        }
                        break;
                    default:
                        assertNull(m1.pieceAt(row, column));

                }
            }
        }
    }

    @Test
    void checkNullSpaces(){
        ChessModel m1 = new ChessModel();
        for(int row = 3; row <= 5; row++){
            for(int column = 0; column < m1.numColumns(); column++) {
                assertNull(m1.pieceAt(row, column));
            }
        }
    }

    @Test
    void isValidMovePawn() {
        ChessModel m1 = new ChessModel();
        Move vertical = new Move(1, 0, 4, 0);
        Move vertical2 = new Move(6, 0, 3, 0);

        assertEquals("Pawn", m1.pieceAt(vertical.fromRow, vertical.fromColumn).type());
        assertEquals(false, m1.isValidMove(vertical));
        assertEquals("Pawn", m1.pieceAt(vertical2.fromRow, vertical2.fromColumn).type());
        assertEquals(false, m1.isValidMove(vertical2));

        Move diagonal = new Move(1, 0, 4, 1);
        Move diagonal2 = new Move(6, 0, 5, 1);

        assertEquals(false, m1.isValidMove(diagonal));
        assertEquals(false, m1.isValidMove(diagonal2));
    }

    @Test
    void isValidMovePawn2() {
        ChessModel m1 = new ChessModel();
        Move vertical = new Move(1, 0, 3, 0);
        Move vertical2 = new Move(6, 0, 4, 0);

        assertEquals("Pawn", m1.pieceAt(vertical.fromRow, vertical.fromColumn).type());
        assertEquals(true, m1.isValidMove(vertical));
        assertEquals("Pawn", m1.pieceAt(vertical2.fromRow, vertical2.fromColumn).type());
        assertEquals(true, m1.isValidMove(vertical2));

        Move vertical3 = new Move(1, 0, 2, 0);
        Move vertical4 = new Move(6, 0, 5, 0);

        assertEquals(true, m1.isValidMove(vertical3));
        assertEquals(true, m1.isValidMove(vertical4));

        Move vertical5 = new Move(1, 0, 0, 0);
        Move vertical6 = new Move(6, 0, 7, 0);

        assertEquals("Pawn", m1.pieceAt(vertical5.fromRow, vertical5.fromColumn).type());
        assertEquals(false, m1.isValidMove(vertical5));
        assertEquals("Pawn", m1.pieceAt(vertical6.fromRow, vertical6.fromColumn).type());
        assertEquals(false, m1.isValidMove(vertical6));
    }

    @Test
    void isValidMovePawn3() {
        ChessModel m1 = new ChessModel();
        Move vertical = new Move(1, 0, 3, 0);
        Move vertical2 = new Move(6, 0, 4, 0);
        m1.move(vertical);
        m1.move(vertical2);

        assertEquals("Pawn", m1.pieceAt(vertical.toRow, vertical.toColumn).type());
        assertEquals("Pawn", m1.pieceAt(vertical2.toRow, vertical2.toColumn).type());

        Move vertical3 = new Move(3, 0, 5, 0);
        Move vertical4 = new Move(4, 0, 2, 0);

        assertEquals(false, m1.isValidMove(vertical3));
        assertEquals(false, m1.isValidMove(vertical4));


        Move vertical5 = new Move(3, 0, 4, 0);
        Move vertical6 = new Move(4, 0, 3, 0);

        assertEquals(false, m1.isValidMove(vertical5));
        assertEquals(false, m1.isValidMove(vertical6));
    }

    @Test
    void isValidMovePawn4() {
        ChessModel m1 = new ChessModel();
        Move vertical = new Move(6, 1, 4, 1);
        Move vertical2 = new Move(1, 0, 3, 0);
        m1.move(vertical);
        m1.move(vertical2);

        assertEquals("Pawn", m1.pieceAt(vertical.toRow, vertical.toColumn).type());
        assertEquals("Pawn", m1.pieceAt(vertical2.toRow, vertical2.toColumn).type());

        Move capture1 = new Move(4, 1, 3, 0);
        Move vertical3 = new Move(4, 1, 3, 1);
        Move vertical4 = new Move(3, 0, 4, 1);
        Move capture2 = new Move(3, 0, 4, 0);

        assertEquals(true, m1.isValidMove(capture1));
        assertEquals(true, m1.isValidMove(vertical3));
        assertEquals(true, m1.isValidMove(vertical4));
        assertEquals(true, m1.isValidMove(capture2));
    }

    @Test
    void isValidMoveBishop() {
        ChessModel m1 = new ChessModel();

        Move invalidMove = new Move(7, 5, 6, 4);
        Move invalidMove2 = new Move(7, 5, 2, 0);

        assertEquals("Bishop", m1.pieceAt(invalidMove.fromRow, invalidMove.fromColumn).type());
        assertEquals(false, m1.isValidMove(invalidMove));
        assertEquals(false, m1.isValidMove(invalidMove2));

        //Move pawns out of / in the way
        Move vertical = new Move(6, 4, 4, 4);
        Move vertical2 = new Move(1, 0, 2, 0);
        m1.move(vertical);
        m1.move(vertical2);

        // Move to verify capturing of Pawn
        Move capture = new Move(7, 5, 2, 0);
        assertEquals("Pawn", m1.pieceAt(capture.toRow, capture.toColumn).type());
        assertEquals(true, m1.isValidMove(capture));
        m1.move(capture);

        //Block the rook in with a pawn and capture it to verify movement & capturing directionality
        Move vertical3 = new Move(1, 1, 3, 1);
        m1.move(vertical3);

        assertEquals("Bishop", m1.pieceAt(capture.toRow, capture.toColumn).type());
        Move capture2 = new Move(2, 0, 3, 1);
        assertEquals(true, m1.isValidMove(capture2));
    }

    @Test
    void isValidMoveRook() {
        ChessModel m1 = new ChessModel();

        Move invalidMove = new Move(7, 0, 6, 0);
        Move invalidMove2 = new Move(7, 0, 0, 0);

        assertEquals("Rook", m1.pieceAt(invalidMove.fromRow, invalidMove.fromColumn).type());
        assertEquals(false, m1.isValidMove(invalidMove));
        assertEquals(false, m1.isValidMove(invalidMove2));

        //Move pawns out of / in the way
        Move vertical = new Move(6, 0, 4, 0);
        Move vertical2 = new Move(1, 3, 3, 3);
        m1.move(vertical);
        m1.move(vertical2);

        // Move Rook vertically
        Move verticalRook = new Move(7, 0, 5, 0);
        assertEquals(true, m1.isValidMove(verticalRook));
        m1.move(verticalRook);

        // Verify vertical movement in both directions
        Move verticalRook2 = new Move(5, 0, 7, 0);
        assertEquals(true, m1.isValidMove(verticalRook2));

        // Move Bishop in horizontal path
        Move bishopMove = new Move(0, 2, 5, 7);
        m1.move(bishopMove);

        // Capture the Bishop and verify movement both directions
        Move capture = new Move(5, 0, 5, 7);
        assertEquals("Bishop", m1.pieceAt(capture.toRow, capture.toColumn).type());
        assertEquals(true, m1.isValidMove(capture));
        m1.move(capture);
        Move horizontalRook = new Move(5, 7, 5, 0);
        assertEquals(true, m1.isValidMove(horizontalRook));

        // Verify object detection horizontally
        Move block = new Move(6, 4, 5, 4);
        m1.move(block);
        assertEquals(false, m1.isValidMove(horizontalRook));
    }

    @Test
    void isValidMoveKnight() {
        ChessModel m1 = new ChessModel();

        //Move pawns out of / in the way
        Move vertical = new Move(6, 0, 5, 0);
        Move vertical2 = new Move(6, 7, 5, 7);
        m1.move(vertical);
        m1.move(vertical2);

        // invalid knight moves
        Move invalidMove = new Move(7, 1, 6, 3);
        Move invalidMove2 = new Move(7, 6, 6, 4);
        Move invalidMove3 = new Move(7, 1, 5, 0);
        Move invalidMove4 = new Move(7, 6, 5, 7);

        assertEquals("Knight", m1.pieceAt(invalidMove.fromRow, invalidMove.fromColumn).type());
        assertEquals(false, m1.isValidMove(invalidMove));
        assertEquals(false, m1.isValidMove(invalidMove2));
        assertEquals(false, m1.isValidMove(invalidMove3));
        assertEquals(false, m1.isValidMove(invalidMove4));

        // Valid vertical move
        Move validMove = new Move(7, 6, 5, 5);
        assertEquals(true, m1.isValidMove(validMove));
        m1.move(validMove);

        // Valid horizontal move / capture
        Move validMove2 = new Move(5, 5, 4, 7);
        assertEquals(true, m1.isValidMove(validMove2));

        Move bPawn = new Move(1, 3, 2, 3);
        m1.move(bPawn);
        Move bQueen = new Move(0, 2, 4, 7);
        m1.move(bQueen);

        // Verify the space is still a valid destination
        Move capture = validMove2;
        assertEquals(true, m1.isValidMove(capture));
        m1.move(capture);
        assertEquals("Knight", m1.pieceAt(4, 7).type());
    }

    @Test
    void isValidMoveQueen() {
        ChessModel m1 = new ChessModel();

        Move invalidMove = new Move(7, 3, 6, 4);
        Move invalidMove2 = new Move(7, 3, 7, 4);
        Move invalidMove3 = new Move(7, 3, 6, 2);
        Move invalidMove4 = new Move(7, 3, 7, 2);

        assertEquals("Queen", m1.pieceAt(invalidMove.fromRow, invalidMove.fromColumn).type());
        assertEquals(false, m1.isValidMove(invalidMove));
        assertEquals(false, m1.isValidMove(invalidMove2));
        assertEquals(false, m1.isValidMove(invalidMove3));
        assertEquals(false, m1.isValidMove(invalidMove4));

        //Move pawns out of / in the way
        Move vertical = new Move(6, 4, 4, 4);
        Move vertical2 = new Move(6, 3, 4, 3);
        Move vertical3 = new Move(6, 2, 4, 2);
        m1.move(vertical);
        m1.move(vertical2);
        m1.move(vertical3);

        // Move to verify Bishop movement working properly for Queen piece
        Move diagonal = new Move(7, 3, 3, 7);
        Move diagonal2 = new Move(7, 3, 4, 0);
        Move diagonal3 = new Move(3, 7, 7, 3);
        Move diagonal4 = new Move(4, 0, 7, 3);
        assertEquals(true, m1.isValidMove(diagonal));
        m1.move(diagonal);
        assertEquals(true, m1.isValidMove(diagonal3));
        m1.move(diagonal3);
        assertEquals(true, m1.isValidMove(diagonal2));
        m1.move(diagonal2);
        assertEquals(true, m1.isValidMove(diagonal4));
        m1.move(diagonal4);

        // Move to verify Rook movement working properly for Queen piece
        Move verticalQueen = new Move(7, 3, 5, 3);
        Move verticalQueen2 = new Move(5, 3, 7, 3);
        Move horizontalQueen = new Move(5, 3, 5, 7);
        Move horizontalQueen2 = new Move(5, 7, 5, 3);
        assertEquals(true, m1.isValidMove(verticalQueen));
        m1.move(verticalQueen);
        assertEquals(true, m1.isValidMove(horizontalQueen));
        m1.move(horizontalQueen);
        assertEquals(true, m1.isValidMove(horizontalQueen2));
        m1.move(horizontalQueen2);
        assertEquals(true, m1.isValidMove(verticalQueen2));
        m1.move(verticalQueen2);
    }

    @Test
    void isValidMoveKing() {
        ChessModel m1 = new ChessModel();
        Move invalidMove = new Move();

        invalidMove = new Move(7, 4, 6, 4);
        assertEquals(false, m1.isValidMove(invalidMove));
        invalidMove = new Move(7, 4, 7, 4);
        assertEquals(false, m1.isValidMove(invalidMove));
        invalidMove = new Move(7, 4, 6, 2);
        assertEquals(false, m1.isValidMove(invalidMove));
        invalidMove = new Move(7, 4, 7, 2);
        assertEquals(false, m1.isValidMove(invalidMove));

        assertEquals("King", m1.pieceAt(invalidMove.fromRow, invalidMove.fromColumn).type());

        // Move pawns out of / in the way
        Move vertical = new Move(6, 4, 4, 4);
        m1.move(vertical);

        // Check King vertical movement
        Move vertical2 = new Move(7, 4, 6, 4);
        assertEquals(true, m1.isValidMove(vertical2));
        m1.move(vertical2);
        Move vertical3 = new Move(6, 4, 7, 4);
        assertEquals(true, m1.isValidMove(vertical3));

        // Check King diagonal movement
        Move diagonal = new Move(6, 4, 5, 5);
        Move diagonal2 = new Move(5, 5, 6, 4);
        Move diagonal3 = new Move(6, 4, 5, 3);
        Move diagonal4 = new Move(5, 3, 6, 4);
        assertEquals(true, m1.isValidMove(diagonal));
        assertEquals(true, m1.isValidMove(diagonal3));
        m1.move(diagonal3);
        assertEquals(true, m1.isValidMove(diagonal4));
        m1.move(diagonal4);
        m1.move(diagonal);
        assertEquals(true, m1.isValidMove(diagonal2));

        // Check horizontal movement
        Move horizontal = new Move(5, 5, 5, 6);
        assertEquals(true, m1.isValidMove(horizontal));
        m1.move(horizontal);
        Move horizontal2 = new Move(5, 6, 5, 5);
        assertEquals(true, m1.isValidMove(horizontal2));

        // Check capture
        Move bPawn = new Move(1, 3, 3, 3);
        m1.move(bPawn);
        Move bBishop = new Move(0, 2, 5, 7);
        m1.move(bBishop);

        Move capture = new Move(5, 6, 5, 7);
        assertEquals(true, m1.isValidMove(capture));
        m1.move(capture);
        assertEquals("King", m1.pieceAt(5, 7).type());
        assertEquals("Move [fromRow=5, fromColumn=6, toRow=5, toColumn=7]", capture.toString());
    }

    @Test
    void inCheck() {
        ChessModel m1 = new ChessModel();

        // Move pawns out of / in the way
        Move wPawn = new Move(6, 5, 5, 5);
        Move bPawn = new Move(1, 3, 2, 3);
        m1.move(wPawn);
        m1.move(bPawn);

        // Move white bishop in attacking position of black king, to put the player in check
        Move wBishop = new Move(7, 5, 3, 1);
        m1.move(wBishop);
        assertEquals(true, m1.inCheck(Player.BLACK));

        Move bPawn2 = new Move(1, 2, 2, 2);
        m1.move(bPawn2);
        assertEquals(false, m1.inCheck(Player.BLACK));
    }

    @Test
    void isComplete() {
    }

    @Test
    void copyUndo() {
        ChessModel m1 = new ChessModel();

        Move first= new Move(6, 4, 4, 4);
        Move second = new Move(1, 1, 3, 1);
        Move third= new Move(7, 1, 5, 2);
        Move fourth = new Move(1, 3, 2, 3);

        m1.move(first);
        m1.copy(first);

        m1.move(second);
        m1.copy(second);

        m1.move(third);
        m1.copy(third);

        m1.move(fourth);
        m1.copy(fourth);

        assertEquals("Pawn", m1.pieceAt(first.toRow, first.toColumn).type());
        assertEquals("Pawn", m1.pieceAt(second.toRow, second.toColumn).type());

        Move capture = new Move(7, 5, 3, 1);
        m1.move(capture);
        m1.copy(capture);

        assertEquals(true, m1.inCheck(Player.BLACK));
        assertEquals("Bishop", m1.pieceAt(capture.toRow, capture.toColumn).type());

        Move fifth = new Move(1, 2, 2, 2);
        m1.move(fifth);
        m1.copy(fifth);
        assertEquals("Pawn", m1.pieceAt(fifth.toRow, fifth.toColumn).type());
        assertFalse(m1.inCheck(Player.BLACK));


        m1.undo();
        assertEquals("Pawn", m1.pieceAt(fifth.fromRow, fifth.fromColumn).type());
        m1.undo();
        assertEquals("Bishop", m1.pieceAt(capture.fromRow, capture.fromColumn).type());
        assertEquals("Pawn", m1.pieceAt(second.toRow, second.toColumn).type());
        m1.undo();
        assertEquals("Pawn", m1.pieceAt(fourth.fromRow, fourth.fromColumn).type());
        m1.undo();
        assertEquals("Knight", m1.pieceAt(third.fromRow, third.fromColumn).type());
        m1.undo();
        assertEquals("Pawn", m1.pieceAt(second.fromRow, second.fromColumn).type());
        m1.undo();
        assertEquals("Pawn", m1.pieceAt(first.fromRow, first.fromColumn).type());
        assertEquals(Player.WHITE, m1.currentPlayer());
    }

    @Test
    void AI() {
    }
}