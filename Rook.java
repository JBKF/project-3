package chess;

import chess.ChessPiece;
import chess.IChessPiece;
import chess.Move;
import chess.Player;

/*****************************************************************
 Sets the parameters for the Rook piece for the game of chess
 @author	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class Rook extends ChessPiece {

	/*****************************************************************
	Constructor that inherits the player passed in by the super class ChessPiece
	@param	player	Enum player that represents a Players color
	 *****************************************************************/
	public Rook(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the Rook ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "Rook";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the Rook chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
		boolean valid = false;

		// Call to the parent method for base validity test
		if(super.isValidMove(move, board) == false){
			return false;
		}

		// Using ternary operators to account for the row and column changes in either direction
		int rowChange = (move.toRow > move.fromRow) ? 1 : -1;
		int colChange = (move.toColumn > move.fromColumn) ? 1 : -1;
		int row = move.fromRow;
		int col = move.fromColumn;

		//Moves horizontally by changing the columns and leaving the row the same
		if (move.toRow == move.fromRow && move.toColumn != move.fromColumn) {
			valid = true;
			// Checks for any objects obstructing the horizontal path
			while(col != move.toColumn){
				col = col + colChange;
				if(board[move.toRow][col] != null && board[row][col] != board[move.toRow][move.toColumn]){
					valid = false;
				}
			}
		}
		//Moves vertically by changing the row value and leaving the column the same
		else if (move.toColumn == move.fromColumn && move.toRow != move.fromRow) {
			valid = true;
			// Checks for any objects obstructing the vertical path
			while(row != move.toRow){
				row = row + rowChange;
				if(board[row][move.toColumn] != null && board[row][col] != board[move.toRow][move.toColumn]){
					valid = false;
				}
			}
		}
		return valid;
	}
}
