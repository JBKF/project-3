package chess;

import chess.ChessPiece;
import chess.IChessPiece;
import chess.Move;
import chess.Player;

/*****************************************************************
 Sets the parameters for the Bishop piece for the game of chess
 @author	Jordan Brown, Kendra Francis
 @version	1.0
 *****************************************************************/
public class Bishop extends ChessPiece {

	/*****************************************************************
	 Constructor that inherits the player passed in by the super class ChessPiece
	 @param	player	Enum player that represents a Players color
	 *****************************************************************/
	public Bishop(Player player) {
		super(player);
	}

	/*****************************************************************
	 Returns the name of the Bishop ChessPiece
	 @return	A String that represents the name of the piece
	 *****************************************************************/
	public String type() {
		return "Bishop";
	}

	/*****************************************************************
	 Determines if a passed in move is valid for the Bishop chess piece
	 @param 	move	location of the piece and its desired destination
	 @param 	board	a 2d array that represents the chess board
	 @return	boolean value that represents the validity of the move
	 *****************************************************************/
	public boolean isValidMove(Move move, IChessPiece[][] board) {
        boolean valid = false;

        // Verifies the change in row and column are equal in order to validate the move for the Bishop chess piece
	    if(super.isValidMove(move, board) && Math.abs(move.toRow - move.fromRow) == Math.abs(move.toColumn - move.fromColumn)) {
	    	// Using ternary operators to account for the row and column changes in either direction
            int rowChange = (move.toRow > move.fromRow) ? 1 : -1;
            int colChange = (move.toColumn > move.fromColumn) ? 1 : -1;
            int row = move.fromRow;
            int col = move.fromColumn;
            valid = true;

			// Checks for any objects obstructing the path
            while(row != move.toRow && col != move.toColumn) {
                row = row + rowChange;
                col = col + colChange;
                if(board[row][col] != null && board[row][col] != board[move.toRow][move.toColumn]) {
					valid = false;
                }

            }
        }
		return valid;
	}
}
