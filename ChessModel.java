package chess;

import java.io.InvalidObjectException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Stack;

/*****************************************************************
 ChessModel Class in which contains the game logic for Chess
 @author Jordan Brown and Kendra Francis
 @version 1.0
 *****************************************************************/
public class ChessModel implements IChessModel {
	/** Instance Variables */

	/** 2D IChessPiece array that represents the chess board */
    private IChessPiece[][] board;

    /** Player object that represents the current player */
	private Player player;

	/** Stack used to track the moves that have been made throughout the chess match */
	private Stack<Move> stackOfMoves = new Stack<>();
    private ArrayList<String> capturedPieces = new ArrayList<>();

	/*****************************************************************
	 Default Constructor - Sets the initial
	 *****************************************************************/
	public ChessModel() {
		// Sets the chess board dimensions
		setBoard();
		// Create a player for the chess game
		setPlayer();

		// Iterate through twice to set the ChessPieces for both players
		for(int i = 0; i < 2; i++)
		{
			setPieces();
			setNextPlayer();
		}

	}

	/*****************************************************************
	 Sets all the pieces on the board
	 *****************************************************************/
	public void setPieces()
	{
        if (this.player == Player.WHITE || this.player == Player.BLACK) {
            switch (this.currentPlayer()) {
                case WHITE:
                    board[7][0] = new Rook(this.player);
                    board[7][1] = new Knight(this.player);
                    board[7][2] = new Bishop(this.player);
                    board[7][3] = new Queen(this.player);
                    board[7][4] = new King(this.player);
                    board[7][5] = new Bishop(this.player);
                    board[7][6] = new Knight(this.player);
                    board[7][7] = new Rook(this.player);

                    for (int col = 0; col < board[0].length; col++) {
                        board[6][col] = new Pawn(this.player);
                    }
                    break;
                case BLACK:
                    board[0][0] = new Rook(this.player);
                    board[0][1] = new Knight(this.player);
                    board[0][2] = new Bishop(this.player);
                    board[0][3] = new Queen(this.player);
                    board[0][4] = new King(this.player);
                    board[0][5] = new Bishop(this.player);
                    board[0][6] = new Knight(this.player);
                    board[0][7] = new Rook(this.player);

                    for (int col = 0; col < board[0].length; col++) {
                        board[1][col] = new Pawn(this.player);
                    }
                    break;
            }
        }
	}

	/*****************************************************************
	 Sets the dimensions of the chess board
	 *****************************************************************/
	public void setBoard()
	{
		this.board = new IChessPiece[8][8];
	}

	/*****************************************************************
	 Set the initial player to the color white
	 *****************************************************************/
	public void setPlayer()
	{
			this.player = Player.WHITE;
	}

	/*****************************************************************
	 Gives the color value of the current Player
	 @return	the current player color
	 *****************************************************************/
	public Player currentPlayer()
	{
		return player;
	}

	/*****************************************************************
	 Checks if a ChessPieces move is valid for the given piece and its current location
	 @param	move	the to and from coordinates for a ChessPiece
	 @return	boolean value that represents the validity of a Move
	 *****************************************************************/
	public boolean isValidMove(Move move) {

		//Checks to make sure there is a piece and that the piece belongs to the current player
		//board[move.fromRow][move.fromColumn] != board[move.toRow][move.toColumn]
		if (board[move.fromRow][move.fromColumn] != null) {
			if (board[move.fromRow][move.fromColumn].isValidMove(move, board)) {
				copy(move);
				return true;
			}
		}
		return false;
	}

	/*****************************************************************
	 Moves the ChessPiece from the row, column coordinates to its desired space
	 @param	move	the to and from coordinates for a ChessPiece
	 *****************************************************************/
	public void move(Move move) {
        if(board[move.toRow][move.toColumn] != null)
        {
            capturedPieces.add(board[move.toRow][move.toColumn].type());
        }
        else{
            capturedPieces.add(null);
        }
		this.board[move.toRow][move.toColumn] = board[move.fromRow][move.fromColumn];
		this.board[move.fromRow][move.fromColumn] = null;
	}

	/*****************************************************************
	 Checks whether or not the passed in Players King ChessPiece is
	 currently under attack
	 @param	p	Player that we want to check the 'Check' status for
	 @return	boolean value that represents whether a Player is in check
	 *****************************************************************/
	public boolean inCheck(Player p) {
		boolean valid = false;
		IChessPiece king = null;
		Move moveToKing = new Move();

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				while (king == null) {
					if (board[i][j] == null || board[i][j].player() != p || board[i][j].type() != "King") {
						break;
					} else {
						moveToKing.toRow = i;
						moveToKing.toColumn = j;
						king = board[i][j];
						i = 0;
						j = 0;

						//Output to display the kings location
						//System.out.println("Player " + king.player() + "'s King is located at row: " + moveToKing.toRow + " Col: " + moveToKing.toColumn);
					}
				}

				if (board[i][j] != null && king != null && board[i][j].player() != king.player()) {
					moveToKing.fromRow = i;
					moveToKing.fromColumn = j;

					//Output to see what is going on
					//System.out.println("" +board[i][j].player()+ " " +board[i][j].type());
					//System.out.println(moveToKing.toString());

					if(board[i][j].isValidMove(moveToKing, board)) {
						valid = true;
					}
				}
			}
		}
		return valid;
	}

	/*****************************************************************
	 Method
	 Check to see if the King is checkmated or can move out of the way
	 or if another piece can block the check
	 Must display a message using JOptionPane.showMessageDialog()
	 See step 10 on PDF for more info
	 @return	boolean value that represents if a Player has any further moves when in 'Check'
	 *****************************************************************/
	public boolean isComplete() {
		boolean valid = false;

		if(inCheck(currentPlayer()) == true){
			for(int row = 0; row < board.length; row++){
				for(int column = 0; column < board[0].length; column++){
					if(board[row][column] != null && board[row][column].player() == currentPlayer())
					{
                        for(int i = 0; i < board.length; i++){
                            for(int j = 0; j < board[0].length; j++) {
                                if(board[i][j] == null && board[i][j].player() != currentPlayer()){
                                    Move available = new Move(row, column, i, j);
                                    if(board[row][column].isValidMove(available, board)){
                                        move(available);
                                        if(inCheck(currentPlayer()) == true)
                                        {
                                            undo();
                                        }
                                        else{
                                            valid = true;
                                        }
                                    }
                                }
                            }
                        }
					}
				}
			}
		}
		return valid;
	}

	/*****************************************************************
	 Gives the number of rows of the chess board
	 @return	integer that represents the number of rows
	 *****************************************************************/
	public int numRows() {
		return 8;
	}

	/*****************************************************************
	 Gives the number of columns of the chess board
	 @return	integer that represents the number of columns
	 *****************************************************************/
	public int numColumns() {
		return 8;
	}

	/*****************************************************************
	 Provides the ChessPiece that is at a provided location
	 @param row	integer that represents the row location of a ChessPiece
	 @param column integer that represents the column location of a ChessPiece
	 @return	the ChessPiece that is located at the provided location
	 *****************************************************************/
	public IChessPiece pieceAt(int row, int column){
	    return board[row][column];
	}

	/*****************************************************************
	 Sets the Player whose turn is next
	 *****************************************************************/
	public void setNextPlayer() {
		this.player = player.next();
	}

	/*****************************************************************
	 Copies the last Move made in the chess match
	 @param	lastMove    the coordinates of the last move made in the chess match
	 *****************************************************************/
	public void copy(Move lastMove){
		// Push the lastMove made to the stack
		stackOfMoves.push(lastMove);

        // Output the move represented by the toString method
        System.out.println(lastMove.toString() + " copied.");
	}


	/*****************************************************************
	 Undoes the last Move made in the chess match
	 *****************************************************************/
	public void undo(){
	    int numMoves = stackOfMoves.size() - 1;

	    // Check if the stack is empty
	    if(numMoves >= 0) {
            // Pop the last Move off the stack
            Move lastMove = stackOfMoves.pop();
            // Output the move represented by the toString method
            System.out.println(lastMove.toString() + " undone.");

            // Set the location of the piece to its destination from its previous move and set the destination to its previous value
            this.board[lastMove.fromRow][lastMove.fromColumn] = board[lastMove.toRow][lastMove.toColumn];

            // Output the player whose turn it is
            Player playerUp = board[lastMove.fromRow][lastMove.fromColumn].player();
            System.out.println("Player " + playerUp + " is now up.");

            if(capturedPieces.get(numMoves) != null && (currentPlayer() == Player.WHITE || currentPlayer() == Player.BLACK)){
                //board[lastMove.toRow][lastMove.toColumn] =
                switch(capturedPieces.get(numMoves)) {
                    case "Pawn":
                        board[lastMove.toRow][lastMove.toColumn] = new Pawn(this.player);
                        break;
                    case "Rook":
                        board[lastMove.toRow][lastMove.toColumn] = new Rook(this.player);
                        break;
                    case "Knight":
                        board[lastMove.toRow][lastMove.toColumn] = new Knight(this.player);
                        break;
                    case "Bishop":
                        board[lastMove.toRow][lastMove.toColumn] = new Bishop(this.player);
                        break;
                    case "Queen":
                        board[lastMove.toRow][lastMove.toColumn] = new Queen(this.player);
                        break;
                }
            }
            else{
                board[lastMove.toRow][lastMove.toColumn] = null;
            }
        }
	    //Set the next player
        this.setNextPlayer();
	}

	/*****************************************************************
	 Method
	 @param
	 @return
	 @throws
	 @see
	 *****************************************************************/
	public void AI() {
		/*
		 * Write a simple AI set of rules in the following order. 
		 * a. Check to see if you are in check.
		 * 		i. If so, get out of check by moving the king or placing a piece to block the check 
		 * 
		 * b. Attempt to put opponent into check (or checkmate). 
		 * 		i. Attempt to put opponent into check without losing your piece
		 *		ii. Perhaps you have won the game. 
		 *
		 *c. Determine if any of your pieces are in danger, 
		 *		i. Move them if you can. 
		 *		ii. Attempt to protect that piece. 
		 *
		 *d. Move a piece (pawns first) forward toward opponent king 
		 *		i. check to see if that piece is in danger of being removed, if so, move a different piece.
		 */

		}
}
